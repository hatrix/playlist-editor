from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

from tinytag import TinyTag
from pathlib import Path
from pprint import pprint

import sys
import os

class MusicListWidget(QListWidget):
    def __init__(self, playlists, tableWidget):
        super(MusicListWidget, self).__init__()
        self.items_tuple = []
        self.playlists = playlists
        self.tableWidget = tableWidget

    def mousePressEvent(self, QMouseEvent):
        event = QMouseEvent

        if QMouseEvent.button() == Qt.LeftButton:
            x = event.x()
            y = event.y()
            selected_item = self.itemAt(x, y)
            if selected_item:
                self.setCurrentItem(selected_item)

        elif QMouseEvent.button() == Qt.RightButton:
            x = event.x()
            y = event.y()
            selected_item = self.itemAt(x, y)
            if selected_item:
                # Loop through items to know if it's a song or not
                for it, song, full_path, album, artist in self.items_tuple:
                    if it == selected_item and song:
                        text = selected_item.text().strip()
                        self.setCurrentItem(selected_item)

                        # Get selected playlist
                        index = self.tableWidget.currentIndex()
                        p = self.tableWidget.tabText(index)

                        # Add song to selected playlist
                        if artist not in self.playlists[p]:
                            self.playlists[p][artist] = dict()
                        if album not in self.playlists[p][artist]:
                            self.playlists[p][artist][album] = list()
    
                        # Keep it unique
                        if text not in self.playlists[p][artist][album]:
                            self.playlists[p][artist][album].append(text)
                            
                            # Update tab list
                            self.tableWidget.lists[p].addItem(full_path)
                        break
                    if it == selected_item and not song:
                        print("A directory can't be added to a playlist")
                        break


    def addItem(self, label, is_song, full_path=None, album=None, artist=None):
        # label: string
        # song: bool
        # full_path: string
        # 
        # This function adds an item to the list
        # It also tracks in a separate python list if that item is about 
        # a song or not (i.e. dir or song)
        super(MusicListWidget, self).addItem(label)

        item_song_tuple = (self.item(len(self.items_tuple)), 
                           is_song, 
                           full_path,
                           album,
                           artist)
        self.items_tuple.append(item_song_tuple)
    

class PlaylistListWidget(QListWidget):
    def __init__(self, playlists, tableWidget):
        super(PlaylistListWidget, self).__init__()
        self.playlists = playlists
        self.tableWidget = tableWidget
    
    def mousePressEvent(self, QMouseEvent):
        event = QMouseEvent

        if QMouseEvent.button() == Qt.LeftButton:
            x = event.x()
            y = event.y()
            selected_item = self.itemAt(x, y)
            if selected_item:
                self.setCurrentItem(selected_item)

        elif QMouseEvent.button() == Qt.RightButton:
            x = event.x()
            y = event.y()
            selected_item = self.itemAt(x, y)
            if selected_item:
                # Select the item
                self.setCurrentItem(selected_item)

                # Get selected playlist
                index = self.tableWidget.currentIndex()
                p = self.tableWidget.tabText(index)

                # Get current song
                text = selected_item.text().strip()
                song = os.path.basename(text)

                # Iterate on the playlist to find our song
                for artist in self.playlists[p]:
                    for album in self.playlists[p][artist]:
                        if song in self.playlists[p][artist][album]:

                            # Remove the song
                            self.playlists[p][artist][album].remove(song)
                            self.takeItem(self.currentRow())

                            # Remove album or artist if now empty
                            if len(self.playlists[p][artist][album]) == 0:
                                del self.playlists[p][artist][album]
                            if len(self.playlists[p][artist]) == 0:
                                del self.playlists[p][artist]

                            return



class TableWidget(QTabWidget):
    def __init__(self, parent, playlists):
        super(QWidget, self).__init__(parent)

        # Initialize tabs and list
        self.tabs = dict()
        self.lists = dict()
        
        # Store a pointer to the playlists
        self.playlists = playlists

    def add_tab(self, name):
        # Add tab
        self.tabs[name] = QWidget()
        self.addTab(self.tabs[name], name)

        # Add lists
        self.lists[name] = PlaylistListWidget(self.playlists, self)

        # Get current tab
        tab = self.tabs[name]
        
        # Add listing to tab
        tab.layout = QVBoxLayout(tab)
        tab.layout.addWidget(self.lists[name])
        tab.setLayout(tab.layout)
    


def get_args():
    if len(sys.argv) != 3:
        print("Usage:")
        print("  ./editor.py music_path playlists_path")
        exit(1)
    return sys.argv[1], sys.argv[2]


def add_songs_to_list(path, listWidget, exclude_keywords=[]):
    tree = {}
    for root, d_names, f_names in os.walk(path, followlinks=True):
        add_dir = False
        songs = []
        for f in sorted(f_names):
            try:
                song_path = os.path.join(root, f)
                if any([kw for kw in exclude_keywords if kw in song_path]):
                    continue

                if song_path.endswith(('mp3', 'flac', 'ogg', 'wav','aac')):
                    songs.append(song_path)
                    add_dir = True
            except:
                pass

        if add_dir:
            artist = Path(root).parent.parts[-1]
            if artist not in tree:
                tree[artist] = dict()

            album = os.path.basename(root)
            if album not in tree[artist]:
                tree[artist][album] = list()

            for song_path in songs:
                song = os.path.basename(song_path)
                tree[artist][album].append((song, song_path))
            

    # addItem: label, is_song, full_path, album, artist
    for artist in sorted(tree.keys()):
        listWidget.addItem(artist, False)

        for album in sorted(tree[artist].keys()):
            listWidget.addItem(" " * 4 + album, False)

            for song, full_path in tree[artist][album]:
                listWidget.addItem(" " * 8 + song, 
                                   True, 
                                   full_path=full_path,
                                   album=album,
                                   artist=artist)


def open_playlists(playlists_path):
    # Get filenames and playlist names
    files = os.listdir(playlists_path)
    files = [f for f in files if f.endswith('.m3u')]

    playlists = dict()

    for f in files:
        lines = open(os.path.join(playlists_path, f)).readlines()
        lines = [l.strip() for l in lines]

        # Check that we don't get any http link
        do_break = False
        for l in lines:
            if l.startswith('http'):
                # let's get out of here
                do_break = True

        if do_break:
            continue

        name = f[:-4].replace('_', ' ').title()

        if name not in playlists:
            playlists[name] = dict()

        for path in lines:
            song = os.path.basename(path)
            p = Path(path)
            album = p.parent.parts[-1]
            artist = p.parent.parts[-2]

            if artist not in playlists[name]:
                playlists[name][artist] = dict()
            if album not in playlists[name][artist]:
                playlists[name][artist][album] = list()

            playlists[name][artist][album].append(song)

    return playlists


def add_songs_to_tabs_lists(music_path, playlists, tableWidget):
    # Update tab list
    for name in playlists:
        for artist in playlists[name]:
            for album in playlists[name][artist]:
                for song in playlists[name][artist][album]:
                    path = os.path.join(artist, album, song)
                    full_path = f"{music_path}{path}"
                    tableWidget.lists[name].addItem(full_path)


def save_playlists(playlists, playlists_path):
    for name in playlists:
        original_name = '{}.m3u'.format(name.replace(' ', '_')).lower()
        play_path = os.path.join(playlists_path, original_name)

        f = open(play_path, 'w')
        for artist in playlists[name]:
            for album in playlists[name][artist]:
                for song in playlists[name][artist][album]:
                    path = os.path.join(artist, album, song)
                    f.write(f'{path}\n')

        print(f'Saved playlist {name}')
        f.close()


def main():
    music_path, playlists_path = get_args()

    # Create App
    app = QApplication([])
    
    # Create main window widget
    win = QWidget()
    win.setGeometry(0, 0, 1300, 800)
    win.setWindowTitle("hat's Playlist Editor")
    
    # Create playlists holder for widgets
    # playlists[playlist_name] 
    #   artist 
    #     album
    #       song
    #       song
    #       ....
    playlists = dict()

    # Update playlists
    playlists = open_playlists(playlists_path)

    # Create other widgets
    tableWidget = TableWidget(win, playlists)
    listWidget = MusicListWidget(playlists, tableWidget)
    
    # Add dummy playlists as tabs
    for playlist in playlists:
        tableWidget.add_tab(playlist)
    
    # Add songs from opened playlists to ListWidget
    add_songs_to_tabs_lists(music_path, playlists, tableWidget)

    # Setup the menu and triggers for saving
    saveAction = QAction("Save", win)
    saveAction.setShortcut("Ctrl+s")
    saveAction.triggered.connect(
             lambda foo, 
                playlists=playlists, path=playlists_path: 
                save_playlists(playlists, path)
    )

    quitAction = QAction("Quit", win)
    quitAction.setShortcut('Ctrl+q')
    quitAction.triggered.connect(app.quit)

    mainMenu = QMenuBar()
    actionFile = mainMenu.addMenu("File")
    actionFile.addAction(saveAction)
    actionFile.addAction(quitAction)

    # Create the grid layout and add the widgets to it
    grid = QGridLayout()
    grid.addWidget(mainMenu, 0, 0, 1, 2)
    grid.addWidget(listWidget, 1, 0)
    grid.addWidget(tableWidget, 1, 1)
    win.setLayout(grid)
    

    # Add songs from music library to the list on the left
    add_songs_to_list(music_path, listWidget, exclude_keywords=["music_dl"])

    # Show the main widget
    win.show()
    sys.exit(app.exec_())
	
if __name__ == '__main__':
    main()
